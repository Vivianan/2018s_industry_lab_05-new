package ictgradschool.industry.abstraction.farmmanager.animals;


public class Duck extends Animal  {
    private final int MAX_VALUE = 400;

    public Duck() {
        value = 250;
    }

    public void feed() {
        if (value < MAX_VALUE) {
            value=value+(MAX_VALUE-value)/2;
        }
    }

    public int costToFeed() {
        return 5;
    }

    public String getType() {
        return "Duck";
    }

    public String toString() {
        return getType() + " - $" + value;
    }
}
